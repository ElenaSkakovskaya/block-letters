import './Letter.css';
import React from 'react';

class Letter extends React.Component {
  constructor(props){
    super(props);
    this.state= {
      letter: props.content || ''
    };
    this.changeContent = () => {
      this.setState({
        letter: props.onClick()
      });
    };
  };
  render() {
    return <span className="letter" onClick={this.changeContent}>{ this.state.letter }</span>
  }
}
export default Letter;