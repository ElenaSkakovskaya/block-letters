import Letter from '../views/Letter';
import logo from '../assets/logo.svg';
import Phrase from '../views/Phrase';
import React, { Component } from 'react';
import Operation from '../helpers/StringOps';
import './App.css';

class App extends Component {
  constructor(props){
    super(props);
    this.state = {
      word: '',
      pointer: 0
    };
    this.getWord = (url) => {
      const regUrl = /^.*?.*word=([a-zA-Z0-9\s,!:;$]*)&?.*$/gi.exec(decodeURI(url));
      return regUrl ? regUrl[1] : "";
    };
    this.pushWords = () => {
      this.setState(prev => ({
          pointer: ++prev.pointer
      }));
      return this.state.word[this.state.pointer];
    };
    this.revertWord = () => {
      this.newWord = new Operation(this.state.word);
      this.newWord.revertStr();
      this.setState({
        word: this.newWord.word,
        pointer: 0
      }, () => {console.log('revertWord ' + this.newWord.word);});
    };
    this.upperWord = () => {
      this.newWord = new Operation(this.state.word);
      this.newWord.upperStr();
      this.setState({
        word: this.newWord.word,
        pointer: 0
      }, () => {console.log('upperWord ' + this.newWord.word);});
    };
    this.revertUpperWord = () => {
      this.newWord = new Operation(this.state.word);
      this.newWord.revertStr().upperStr();
      this.setState({
        word: this.newWord.word,
        pointer: 0
      }, () => {console.log('revertUpperWord ' + this.newWord.word);});
    };
    this.resetWord = () => {
      this.setState({
        word: this.getWord(window.location),
        pointer: 0
      }, () => {console.log('resetWord ' + this.state.word);});
    };
  }
  componentDidMount() {
    this.setState({
      word: this.getWord(window.location)
    });
  }
  render() {
    return (
      <div className="App">
        <header className="App-header">
          <img src={logo} className="App-logo" alt="logo" />
          <h1 className="App-title">Welcome to React</h1>
        </header>
        <div className="App-intro">
          <Phrase content={this.state.word} />
          <Letter onClick={this.pushWords} />
          <Letter onClick={this.pushWords} />
          <div className="App-operations">
            <p className="App-operation">
              <button onClick={this.revertWord}>Reverse phrase</button>
            </p>
            <p className="App-operation">
              <button onClick={this.upperWord}>Uppercase phrase</button>
            </p>
            <p className="App-operation">
              <button onClick={this.revertUpperWord}>Reverse and uppercase phrase</button>
            </p>
            <p className="App-operation">
              <button onClick={this.resetWord}>Reset phrase</button>
            </p>
          </div>
        </div>
      </div>
    );
  }
}

export default App;
