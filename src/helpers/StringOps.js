export const Operation = function (word) {
  this.word = word;
  this.revertStr = function(){
    this.word = this.word.split("").reverse().join("");
    return this;
  };
  this.upperStr = function(){
    this.word = this.word.toUpperCase();
    return this;
  };
};

export default Operation;