import './Phrase.css';
import Letter from './Letter';
import React from 'react';

const Phrase = (props) => {
  const letters = props.content.split('');
  const letterItems = letters.map((letter, key) =>
    <Letter content = {letter} key = {key} />
  );
  return (
    <p>{letterItems}</p>
  );
};
export default Phrase;