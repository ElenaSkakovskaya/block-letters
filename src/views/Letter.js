import './Letter.css';
import React from 'react';

const Letter = (props) => (<span className="letter">{ props.content }</span>);
export default Letter;